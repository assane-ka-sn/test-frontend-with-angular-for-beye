export interface RequestProduct {
  brandId: number;
  name: string;
  description: string;
}

export interface Product {
  id: number;
  md5: string;
  url: string;
  categories: Array<Category>;
  brand: Brand;
  name: string;
  description: string;
}


export interface Category {
  id: number;
  name: string;
}

export interface Brand {
  id: number;
  name: string;
}
