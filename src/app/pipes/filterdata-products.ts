/**
 * Created by PC on 18/10/2018.
 */
import * as _ from "lodash";
import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: "dataFilterProducts"
})
export class DataFilterProductsPipe implements PipeTransform {

  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row=>{
        return  (row.name.toLowerCase().indexOf(query.toLowerCase())>-1 ||
          row.description.toLowerCase().indexOf(query.toLowerCase())>-1 ||
          row.brandname.toLowerCase().indexOf(query.toLowerCase())>-1
        ) });
    }
    return array;
  }
}

