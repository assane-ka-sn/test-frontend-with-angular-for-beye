import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DetailComponent} from "./detailComponent/detail.component";
import {DashboardComponent} from "./dashboardComponent/dashboard.component";

const routes: Routes = [
  { path: '', component: DashboardComponent},
  { path: 'detail/:id', component: DetailComponent},
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
