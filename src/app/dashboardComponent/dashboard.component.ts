import {Component, OnInit, OnDestroy, TemplateRef} from '@angular/core';
import {ApiService} from "../api-server";
import {Router} from "@angular/router";
import {Brand, Category, Product, RequestProduct} from "../interfaces";
import {BsModalService, BsModalRef} from "ngx-bootstrap";


@Component({
  selector: 'app-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.css'],
  providers: [ApiService]
})
export class DashboardComponent implements OnInit, OnDestroy {

  public rowsOnPageProducts = 10;
  public sortByProducts = "name";
  public sortOrderProducts = "desc";
  public filterQueryProducts:any;
  public listeProducts:any[] = [];
  public listBrands:any[] = [];
  public modalRef: BsModalRef;
  public newNameProduct: string;
  public newDescriptionProduct: string;
  public newBrandIdProduct: string="0";

  constructor(private router: Router, private api: ApiService, private modalService: BsModalService)  {  }

  ngOnInit() {
    this.listProducts();
  }

  ngOnDestroy() {
  }

  public openModalNewProduct(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  public openModal(template: TemplateRef<any>,product: Product) {
    this.modalRef = this.modalService.show(template);
  }

  public closeModal() {
    this.modalRef.hide()
  }

  public listeBrands(){
    this.api.listeBrands().subscribe(
      (data:Brand[]) => {
        console.log(data)
        this.listBrands = data
      },
      error => {
        console.log(error);
      },
      () => console.log("slf")
    );
  }

  public listCategories(){
    this.api.listeCategories().subscribe(
      (data:Category[]) => {
        console.log(data)
      },
      error => {
        console.log(error);
      },
      () => console.log("slf")
    );
  }

  public listProducts(){
    this.api.listeProducts().subscribe(
      (data:Product[]) => {
        console.log(data)
        this.listeProducts = data.map(function (type) {
          return {
            id:type.id,
            name:type.name,
            brand:type.brand,
            brandname:type.brand.name,
            description:type.description,
            categories:type.categories
          }
        });
      },
      error => {
        console.log(error);
      },
      () => this.listeBrands()
    );
  }

  public deleteProducts(product){
    this.api.deleteProduct(product.id).subscribe(
      data => {
        console.log(data)
      },
      error => {
        console.log(error);
      },
      () => this.listProducts()
    );
  }

  addproduct(){
    console.log(this.newNameProduct+" "+this.newDescriptionProduct+" "+this.newBrandIdProduct)
    this.api.addProduct({name:this.newNameProduct, description:this.newDescriptionProduct, brandId:Number(this.newBrandIdProduct)}).subscribe(
      data => {
        console.log(data)
        this.listProducts();
        this.closeModal();
      },
      error => {
        console.log(error);
      },
      () => console.log("slf")
    );
  }

}
