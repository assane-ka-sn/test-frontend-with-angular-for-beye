import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {DetailComponent} from "./detailComponent/detail.component";
import {DashboardComponent} from "./dashboardComponent/dashboard.component";
import {DataTableModule} from "angular2-datatable";
import { ModalModule } from "ngx-bootstrap";

import {ApiService} from "./api-server";
import {DataFilterProductsPipe} from "./pipes/filterdata-products";

@NgModule({
  declarations: [
    AppComponent,
    DetailComponent,
    DashboardComponent,

    DataFilterProductsPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    DataTableModule,
    ModalModule.forRoot(),

  ],
  providers: [
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
