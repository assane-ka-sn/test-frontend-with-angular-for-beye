import { Injectable }    from '@angular/core';
import { HttpClient,HttpHeaders  } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Brand, Product, Category} from "./interfaces";


@Injectable()
export class ApiService {

  public link = "http://localhost/test-symfony/web";
  private headers: HttpHeaders;

  constructor(private _http: HttpClient){
    this.headers =  new HttpHeaders({'Content-Type':  'application/x-www-form-urlencoded'});
  }

  listeBrands(): Observable<Brand[]>{
    let url = this.link+"/brands/";
    return this._http.get<Brand[]>(url, {headers:this.headers})
  }

  listeCategories(): Observable<Category[]>{
    let url = this.link+"/categories/";
    return this._http.get<Category[]>(url, {headers:this.headers})
  }

  listeProducts(): Observable<Product[]>{
    let url = this.link+"/products/";
    return this._http.get<Product[]>(url, {headers:this.headers})
  }

  addProduct(data): Observable<Product>{
    let url = this.link+"/products";
    return this._http.post<Product>(url, JSON.stringify(data), {headers:new HttpHeaders({'Content-Type':  'application/json'})})
  }

  deleteProduct(productId:number){
    let url = this.link+"/products/"+productId;
    return this._http.delete(url,{headers:this.headers})
  }


}
