import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from "@angular/router";


@Component({
  selector: 'app-detail',
  templateUrl: 'detail.component.html',
  styleUrls: ['detail.component.css'],
  providers: []
})
export class DetailComponent implements OnInit, OnDestroy {

  constructor(private router: Router)  {  }

  ngOnInit() {

  }

  ngOnDestroy() {
  }

}
